# Including
Including source files and/or directing the linker
`#include` will paste the files contents directly.

The following 4 directives can be chained with `#ifdef`, `#if`, etc to generate context-aware/portable compilation and linking options

	#header header.htl // paste header file contents
	#object object.o // link shared object file (dyanimc linking)
	#archive archive.a // link archive file (static linking)
	#source source.ctl // paste source file contents


# Operators

	pointers
		$	address
		@	dereference
		->	follow
		.	offset
	arrays
		.	access // arr.x, also known as arr[x] 
		..	range // a..b from a to b
		+..	seqarith // continue sequence a,b+.. as if arithmetic
		*..	seqgeom // continue sequence a,b*.. as if geometric
	bitwise 	
		|	bor
		&	band
		~	bnot
		^	bxor
		<<	lshift
		>>	rshift
		.<	lrot
		.>	rrot
	arithmetic
		+	plus
		-	min
		/	div
		*	mul
		++	inc 
	// post: adds 1 and returns old value
	// pre: adds 1 and returns new value
		--	dec
		%	mod
	compare
		>	gt
		<	lt
		>=	lte
		<=	gte
		==	eq
		\=	neq
	logic		
		!	not
		&&	and
		||	or
		^^	xor
	assignment
		=	is
	swap nouns
		<>	swap
	conversion
		:	convert
	namespace resolver
		::	nsr
	ternary
		? : ,
	
**Implicit operators**

		!=	isnot
		+=	isplus
		-=	ismin
		*=	ismul
		/=	isdiv
		%=	ismod
		~=	isbnot
		^=	isbxor
		&=	isband
		|=	isbor

## Ternary
	
	(x?a:b,c)

Where x is an expression and a,b,c are all expressions, or empty.

If x, return a, otherwise return b. In any case, also do c. Note that ternary's `:` goes after conversion's `:`. When in doubt, use parentheses.

Other fun valid forms include:

	(x--?:x++,printf("%d",x)) 

# Other characters

	precedence	( )
	code block	{ }
	arrays		[ ] < > ,
	delimiter	;

# Nouns, symbols

CONTROL makes a distinction between a variable (symbol) and what the variable "holds" (a noun).

A variable is a symbol for a noun.

A noun can exist without a corresponding variable -> anonymous noun.

A noun is a discrete unit of data made up of bits.

Nouns have fully qualified types.

Types are primitives, or derived from primitives via typedef, struct, union, enum.

Types have qualifiers which constrain them.

A fully qualified type = type + constraints.

Note that constraints might be an empty set for some nouns

FQ-type: fully qualified type

Operators act on whole nouns.

# main

All CONTROL programs begin execution in the special `main` block.

	main {
		return 0;
	}

You can capture argc, argv, envp

	main (i32 argc, **u8 argv, **u8 envp) {
		return 0;
	}

The `return 0` is implicit in main.

# Types:

CONTROL offers the following primitives from which you can construct all other types:

	u8 u16 u32 u64
	i8 i16 i32 i64
	   f16 f32 f64
	b8 b16 b32 b64

 - The `iX` and `uX` types are resp. signed and unsigned integers of X bits.

 - The `fX` types are IEEE floating points of X bits. There is no `f8`.

 - The `bX` types are bitfields, and individual bits can be accessed with `bit.offset`.

 - To store a blob of binary data, use u8. To store strings, use u8 for UTF-8 or u16 for UTF-16.


**Note:** the type of a literal is deduced from the context.

In C code, you often see things like `float x = 19.f`. In CONTROL, this is just

	f32 x = 19; // will produce floating point number 19.0

## Declaration and modification

You can declare a variable by

	`fq-type name;`

You can initialize it too:

	`fq-type name = initializer;`

Or you can modify variables, call functions, etc:

	`name = modification;`
	`name = function();`

## Type Qualifiers
	
These say something about the noun, not the variable.

[default], alternative

**Mutability:**

	[mut], const

 - `mut` means the noun is mutable, i.e. its data can be modified
 - `const` means the noun is constant, i.e. its data can not be modified

**Storage class:**

	[auto], stack, register, static

 - `auto` lets the compiler choose
 - `stack` puts the variable in the stack frame
 - `register` puts the data in a register
 - `static` puts the data in the data section of a program, so each time the variable is referenced in its scope, the same data will be used. Note this is not the same as a globally accesible variable, it is still limited to the scope.

**Endianness:**
	
	[native], leo, beo, leb, beb, lolb, lobb, bolb, bobb

 - `leo` little endian octet order
 - `beo` big endian octet order
 - `leb` little endian bit order
 - `beb` big endian bit order

 - `lolb` little octet little bit
 - `lobb` little octet big bit
 - `bolb` big octet little bit
 - `bobb` big octet big bit

Network byte order 32-bit integer for instace: `bobb i32`

x86-64 processor native byte order is `lobb`

**Note:** incompatible fq-types must be explicitly converted. `bobb i32 + lolb i32 = compilation error`

## Pointer types

`*T` `uniq* T` `g*`

Pointers are a special type of noun, with the implication that its data translates to an adress of another noun of type `T`. As such, a pointer noun can not stand on its own.

	* mypointer; // fail
	* i32 mypointer = ; // ok
	* u8 unknownData; // "void" pointers are u8 pointers in CONTROL

Pointers always follow the native endianness, so the endianness qualifiers are disqualified. All other type qualifiers are available. Additionally, pointers have a special _singularity_ "qualifier" available, `uniq`. 

	uniq

`uniq` forces the compiler to enfore the constraint that the there is only 1 symbol for the noun, or that the noun is a pattern of zeroes. In effect, only a single variable can point to the adress.

	uniq* i32 mypointer2 = mypointer; // fail, multiple symbols (2) referencing the same noun
	* i32 nullptr = null; // ok
	* i32 nullptr2 = null; // ok because noun = pattern of zeroes, despite nullptr2 not only symbol

In truth, this is just syntax sugar for the 2 kinds of pointers in CONTROL: common pointers `*` and unique pointers `uniq*`.

## Deriving types

You might want to store an ASCII string. ASCII goes into characters of 8 bits, so we use the `u8` primitive:

	typedef ascii u8
	typedef Cstring <'\0'>ascii // null terminated array

You can use a string literal to instantiate u8 arrays or u8 array pointers:

	Cstring greeting = "Hello, world!";
	// same as C's `char[] var = literal;`

	typedef literal <'\0'> const ascii;
	const * literal in_data_section = "Hello, world!";
	// same as C's `char* in_data_section = "Hello, world!";`

You can use `typedef` for trivial types, but you can also create `structs`, `unions` and `enums`.

	typedef utf8 []u8;

	struct organism {
		*utf8 name;
		enum sort {
			plant,
			animal,
		}
		union {
			f32 waterPerYear;
			u8 legCount; 
		}
	}

## Fully qualified types

A fully qualified type is the complete, compound type of a noun. This includes, possibly recursively:

	pointer_qualifiers pointer type_qualifiers type

For instance,

	i32 q1;

The fq-type **of the noun** symbolized by `q1` is: `i32`

	uniq const * const static lobb i32 q2;

fq-type is `uniq const * const static lobb i32`

	*[3][3][3]i32 q3;

fq-type is `*[3][3][3]`

## Casting (interpretation or conversion)

CONTROL offers you to either interpret a bunch of data as if it were a certain type, or convert the data from type a to type b.

For instance, you might want to convert an int to a float. This does not preserve the exact underlying bits of data as the structure of both is different. But it does preserve the numerical value (where possible).

### Conversion

	i32 number = 1;
	f32 conversion = float:number; // conversion is now 1.00000

The conversion operator `:` takes the right hand side and converts the underlying data to match the type of left hand side.

### Interpretation

But you might also want to fill a float with the exact bit pattern of what's held in that i32 variable:

	f32 interpretation = f32 number;

Interpretation does not modify the underlying bits. It just changes the associated type. It is especially handy when parsing data packets or binary blobs.

### Summary

**Conversion (`:`)** lets you modify the type while preserving meaning

**Interpretation** lets you modify the type while preserving raw data

# Arrays

Arrays are handled as a noun.

Wether a noun is an array or not, depends on its type.

## different kinds

There are 3 kinds of arrays: terminated arrays, statically sized arrays, and dynamically sized arrays. You can convert between these types as expected.

## Terminated Array

**A terminated array** goes on until a terminator element is reached. It can be put in the data section of a program or in heap memory or in a stack frame. In memory, it might look like this:
	
	[elem] [elem] [elem] [elem] [terminator]

Note that the terminator is of the same type as the elements.

You may already know this array type from C's `char*`.

	<Terminator>Type var;

## Statically sized array

**A statically sized array** has a compile-time known amount of elements. It can be put in the data section of a program or in heap memory or in a stack frame. In memory it looks like this:

	[elem] [elem] [elem] [elem]

## Dynamically sized array

**A dynamically sized array** has a dynamic amount of elements. It can only be put on the heap or in a stack frame. A 4 element dynamic array looks like this in memory:

	[0x0000000000000004] [elem] [elem] [elem] [elem]

Thus, a 64-bit unsigned integer describing how many elements there are, then the actual elements at offset=8 bytes.

To specify the endian-ness of this number, supply the arraytype itself a qualifier:
	
	lobb[] i32 arr1;
	// the array's count is lobb, the elements are native endian
	lobb[] lobb i32 arr2;
	// both the array's count and the elements are lobb

This approach also works for specifying a terminator's endianness.

**Note:** As you might expect, specifying the endian-ness of an array itself, when the array is static, does nothing.

## Notes

**Note:** All arrays are allocated, wherever they are allocated, as a continuous block, because they are a noun.

**Note:** The address-of operator, `$`, when used on an array, shall return the address of the first byte of a noun. This means that it will not necesarily return the address of the first element, such is the case with dynamic arrays, where it returns the address of the element count.

**Note:** Due to arrays being nouns, using a bitwise operator on an terminated array or dynamic array will also affect the terminator element or the count element respectively.

**Note:** To convert a dynamically sized array to follow the same format as a statically sized array (i.e., drop the u64 prefix), you can convert like so: `[0]type:array`. Keep in mind you must manually allocate memory and keep count then.

## Declaring arrays

 - Declaration of form...   Evaluates to...
 - `[0] type x = literal`    statically  sized array
 - `[]  type x = variable`   dynamically sized array
 - `[0] type x = variable`   FAIL
 - `[literal]  type x = any` statically  sized array
 - `[variable] type x = any` dynamically sized array

## Arrays as function arguments

Due to polymorphism, the exactly type-matching function is executed. 

	// this arrayfunc takes a dynamic array of integers
	fn arrayfunc([]i32 arr){
		printf("Dynamic version");
	}
	
	// this arrayfunc takes a static array of 8 integers
	fn arrayfunc([8]i32 arr){
		printf("Static version");
	}

	arrayfunc([0]i32[1,2,3,4,5,6,7,8]); // static version
	arrayfunc([]i32[1,2,3,4,5,6,7,8]); // dynamic version

	arrayfunc([]i32[1,2,3,4,5,6,7,8,9]);

Will fail because there is no function `arrayfunc` defined that accepts type `[9]i32`.

## Arrays and pointers

**Note:** In C, an array is not a pointer, despite sometimes similar usage syntax. The same holds true for CONTROL.

Pointer to array or array of pointers?

	// array of (pointer to (int))
	i32 var1, var2;
	[]*i32 ptrarr = [$var1, $var2];

	// pointer to (array of (int))
	[]i32 = [var1, var2];
	*[]i32 arrptr = $arr; 

## Multidimensional arrays

	[3][3]i32 arr_2d = [[1,2,3],[4,5,6],[7,8,9]];
	// arr_2d is an (array of 3 (arrays of 3 (ints)))

And you can access nested items with the `.` access operator:

	arr_2d.2.1 == 8; // will evaluate true

Assinging arrays **copies values** rather than a 'pointer to array' like in C.

	[3]i32 subarr = [1337,420,69];
	arr_2d.1 = subarr;
	subarr.2 = 2; // previously 69
	arr_2d.1.2 == 2; // will evaluate false, because arr_2d.1.2 is still 69

To get the length of an array, you can use sizeof(arr)/sizeof(arr[0])
sizeof is compile time, so sometimes it will not work. You must then manually track length.

	printf("test has %d elements!",sizeof(test)/sizeof(test[0]));
	printf("test takes up %d bytes of memory in total!",sizeof(test));

## ranges, sequences and initialization

The range operator `a..b` takes 2 numbers a and b and turns it into an array from a to b.

	[0]u8 arr = 1..5;
	// arr = static [1,2,3,4,5]

	[]u8 arr = 1..5;
	// arr = dynamic [1,2,3,4,5]
	<0>u8 arr = 1..5
	// arr = <1,2,3,4,5><0>

When we initialize an array with a pattern, we can continue the pattern with `...`, `..+`, `..*`, The compiler can detect (fractional) addition and (fractional) multiplication.

	[5]i32 arr = [1,2..+];
	// arr = [1,2,3,4,5]

	[5]i32 arr = [1,2...];
	// arr = [1,2,3,3,3]

	[5]i32 arr = [1,2..*];
	// arr = [1,2,4,8,16]

Because the compiler only looks at the last 2 elements. All of these methods return an array of whatever number type the first range operand is. The array is dynamic by default, but a static array can be created by putting these generators as an initializer of a static array.

## methods

Arrays have a couple of built in methods.

## len

`len()` returns the amount of elements in the array.

 - For static arrays this gets compiled away
 - For dynamic arrays it is a O(1) lookup to the element count
 - For terminated arrays it is a O(n) lookup to count until the terminator is hit

## insert, remove, add, push, shift, pop

`insert()` inserts an element to a specific index, and shifts the rest of the array further back to accomodate it.

`remove()` removes a specific element from an array and shifts the rest of the array forward to fill up the gap.

`push` = insert(0)

`shift` = remove(0)

`add` = insert(array.len())

`pop` = remove(array.len())

## map, filter, reduce

**functor**: operator, lambda or function

 - 1-functor: functor that takes at least 1 input (arguments)
 - 2-functor: functor that takes at least 2 inputs (arguments)

### map

`map` will apply a 1-functor to each element in the array.

It maps a function to an array.

	`[]i32 arr = (1..5).map(++);`
	// arr is [2,3,4,5,6]

map is equivalent to
	
	fn map(arr, functor){
		for elem in arr {
			elem = functor(elem);
		}
		return arr;
	}

### filter

`filter` will, for each element of an array, keep or discard it based on a 1-functor applied to that element.

It filters the array.

	fn even(i32 x){
		return x%2 ? 0 : 1;
	}
	arr = arr.filter(even);
	// arr will be [2, 4, 6]

filter is equivalent to

	fn filter(arr, functor){
		filtered;
		for elem in arr{
			if functor(elem){
				filtered.add(elem);
			}
		}
		return filtered;
	}

### reduce

`reduce` will pass each element of an array to a 2-functor.

It reduces the array to a single thing.

	i32 sum = arr.reduce(+);
	// sum = 2+4+6 = 12

	arr.reduce(print) -> (print(print(2, 4)), 6) -> FAIL

As you can see, reduce produces recursion. `+`, which is `ADD(a,b)` can handle this but not `printf`.

### chaining

We can chain all these expressions into one:

	i32 sum = (1..4).add(5).map(++).filter(even).reduce(+);

A good compiler will make this efficient, i.e. consolidate all the array returns. Armed with a bad compiler, you might be better off writing this sort of stuff manually.

# Control flow

## goto

`goto` `label`

goto can be handy for breaking out of many layers of loops. Otherwise `do` gives the same functionality as `goto loop`.

	label loop;
	goto loop;

## jumps

`jumpto` `comefrom` `checkpoint` `with`

In a sense, jumps feel similar to functions. However, they are not. Instead of allocating a new stackframe, they jump back to the stackframe of "comefrom". It allows for intricate handling of errors. If you have ever used `setjmp` in C, this is that, but prettier.

	checkpoint error i32;
	// special declaration
	// error "stores" the stackframe and cpu stack-related registers
	// the return type is i32

	fn foo {
		jumpto error with 4;
	}

	fn main {
		foo();

		comefrom error with code {
			switch code {
				case 4 {
					printf("Recoverable error 4");
				}
				default {
					printf("Unusual error");
					exit(code);
				}
			}
		}
	}
	

## for loops

`for` `in` `to`

For loops are used to count and or iterate.

	for i32 i=0; i<10; i++ {
		printf("%d", i);
	}

	// is functionally equivalent (`to` is tail-exclusive, i.e. 10 will never be reached)
	for i32 i = 0 to 10 {
		printf("%d", i);
	}

You can easily iterate over arrays, or other structures with `implicit iterators`:

	[10]i32 arr = [0,1,2,3,4,5,6,7,8,9];
	for elem in arr {
		printf("%d", elem);
	}

With an index:

	for elem, i32 i in arr {
		printf("%d: %d", i, elem);
	}

### Explicit iterators

`for` `with` `as`

	// the for loop keeps track of iteration count. The iterator function's job is to solely get the correct item.
	fn iterator(i32 iteration, []i32 array) int { // dynamic
		return array[iteration];
	}
	
	[12]i32 array; // static
	// convert to dyn-array, then iterate
	for []i32:array, i32 i with iterator as item { 
		printf("%d: %d", i, item);
	}

## while do and do while

## break and skip

## switch case

## if/elif/else


	while for break skip do
	switch case fall none

		while condition {
			switch var {
				case 1 {
					skip; // equivalent to misnomer "continue" in most languages
				}
				fall 2 // fall through to next item
				fall 3 {
					// maybe do some prepocessing
				}
				case 4 {
					// final
				}
				none { // var matches none of the above
					break; // break out of the nearest loop
				}
			}
		}

		for int i 0; i<10; i++ {
		}
		
		// code first, condition second
		do {
			// code
			if exitcond {
				break;
			}
		}

		// is equivalent to
		
		do {
			// code
		} while !exitcond;

		do 12 {
			// repeats exactly 12 times, then checks condition, then another 12 times or break, etc
		} while !exitcond
		
		// condition first, code second
		do {
			if exitcond {
				break;
			}
			// code
		}

		// is equivalent to

		while !exitcond {
			// code
		}

# functions

`fn` `lam` `return` `as` `succes` `failure`

Define a function

	fn add(i32 a, i32 b) i32 {
		return a+b;
	}
	i32 result = add(32, 64);

Define a function that returns nothing

	fn print(i32 n) {
		printf("%d", n);
	}
	print(7);

Function with static variable

	fn myfunc(<utf8 '\0'> string) {
		static seperator='';
		// static: variable value will be "kept" between function calls
		// (it will be assigned into the BBS section of the executable rather than on a stackframe)
		printf("%c %s \n", seperator, string);
		seperator=',';
		// first run, seperator is ''
		// all next times, seperator is ','
	}

## succes or failure

Define a function with error checking using the `status` `result` `as` `succes` `failure` keywords.
This works as syntax sugar: the return value is simply a bit larger as it contains both the succes state and the actual returned data. As such, functions that have error checking are not compatible with typical C ABIs: you will not be able to link a CONTROL function that uses error checking to a C binary. Similarly, any libc function will not offer CONTROL error checking.
Under the hood, the status return type is a u8.

	fn parse([]u8 data) result<i32> {
		if data[0] != u8 0xfe {
			return as failure;
		}
		i32 sum = i32 (data[1]) + i32 (data[2]<<8) + i32 (data[3]<<16);
		return sum as succes;
	}

	result<i32> sum = parse([]u8 [0xfe, 0x33, 0x22, 0x11]);
	switch status(sum) {
		case 0 {
			printf("Something went wrong!");
		}
		case 1 {
			printf("All good! %d" value(sum));
		}
	}

	in which status() and value() are macros for extracting the status and value out of a return<T> respectively.

## polymorphic functions

You may define a function multiple times for handling different types. The compiler will automatically resolve the correct function to call from the argument type.

	typedef c_string <u8 '\0'>;
	typedef utf8 []u];
	
	fn print(c_string str){
		printf("Print was called on a ISO C string");
	}
	fn print(utf8 str){
		printf("Print was called on a CONTROL string");
	}
	fn print {
		printf("Print was called without any arguments - huh?");	
	}

	print(c_string"What's up? 1972 here!");
	print(utf8"pop pop skrrrraaa");
	print();

If no type match is found the compilation will just fail.

	print(4.65); // fail

# Custom operators

`operator` `left` `right`

you can add operators for your structs:

	struct vec3{
		f32 x;
		f32 y;
		f32 z;

		constructor(f32 x, f32 y, f32 z) {
			this.x = x;
			this.y = y;
			this.z = z;
		}
	}
	
	// left associativity (left is evalled first)
	operator left + (vec3 lhs, vec3 rhs) vec3 {
		return 	(lhs.x+rhs.x, lhs.y+rhs.y, lhs.z+rhs.z);
	}

	vec3 result = vec3(1,2,3)+vec3(4,5,6);

	// You may not overload type-agnositc operators, such as bitwise operators or assignment.
	// They will ALWAYS act on the raw noun's bits.
	// For this, methods.

# Pointers

`*` `@` `$` `uniq`
	
	*i32 p; 
	// defines p to point to an int
	// currently, p points nowhere
	p == null; 
	// known as NULL in C and nullptr in C++
	i32 a = @p;
	// dereference p / indirection
	// read as "a is at p"
	i32 x = 25;
	uniq* i32 y = $x;
	// defines y as a unique pointer, holds address of (@) x

# Linked list example

	namespace ll {
		// template
		struct node <T> {
			T data;
			*node next;

			// "implicit" iterator
			iterator(*node start) T {
				static *node curr = start;
				
				if curr != null {
					T retval = curr.data;
					curr = curr.next;
					return retval;
				} else {
					break;
				}
			}

			constructor([]T arr) node {
				*node current = $this;

				for elem in arr {
					*node next = *node malloc(sizeof(node)) or error("Could not allocate node!");
					current->next = next;
					current->data = elem;
				}

				free(current->next);
				current->next = null;

				return this;
				// the return statement is reduntant.
			}
		}
	}

	[]i32 arr = [0,1,2,3,4,5,6,7,8,9];
	ll::node<i32> list = ll::node<i32>(arr);
	for data, i in $list {
		printf("Node %d holds %d", i, data);
	}

	// For loops of this form call the __iterator method
	// Construction of a struct of the form struct() calls the __constructor method
