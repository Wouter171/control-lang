# The CONTROL Programming Language

A systems and network programming language designed for ultimate deterministic execution and exact definition. It gives high level tools to solve low level problems elegantly, and does so while offering  granularity over the most minute data details.

One thing CONTROL excels at for instance, is handling and converting between different endiannesses.

Another thing is handling, iterating, interpreting, writing, reading binary blobs.

Another thing CONTROL dominates in, is bitwise manipulation. It offers interfaces unseen in more primitive predecesors such as C.

# Spec

CONTROL will be completely specified, and all behaviour will be either defined or illegal.

Stuff like integer overflows and shifts will be fully defined, unlike in C, in which it is undefined behaviour.

Currently, the specification is a work in progress. It will present itself in /docs/spec.md when the time is right.