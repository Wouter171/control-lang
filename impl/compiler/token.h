enum ctl_token_t {
	ctl_constraint,
	ctl_symbol,
	ctl_literal,
	ctl_primitive,
	ctl_keyword,
	ctl_operator,
	ctl_control,
	ctl_no_token
};

enum ctl_constraint_t {ctl_endian_c, ctl_storage_c, ctl_mutab_c};

enum ctl_symbol_t {ctl_function_s, ctl_variable_s, ctl_type_s, ctl_field_s};

enum ctl_literal_t {
	ctl_dec, ctl_dec_hex, 
	ctl_int, ctl_int_oct, ctl_int_hex, ctl_int_bin,
	ctl_string
};

enum ctl_primitive_t {
		ctl_i64, ctl_i32, ctl_i16, ctl_i8,
		ctl_u64, ctl_u32, ctl_u16, ctl_u8,
		ctl_f64, ctl_f32, ctl_f16, ctl_f8,
		ctl_b64, ctl_b32, ctl_b16, ctl_b8,
		ctl_ptr, ctl_uniqptr, ctl_sarr, ctl_darr, ctl_tarr,
		ctl_union, ctl_struct, ctl_enum
	};

enum ctl_keyword_t {
	ctl_fn,
	ctl_lam,
	ctl_return,
	ctl_succes,
	ctl_failure,

	ctl_goto,
	ctl_label,
	ctl_jumpto,
	ctl_comefrom,
	ctl_checkpoint,
	ctl_with,
	ctl_for,
	ctl_in,
	ctl_to,
	ctl_as,
	ctl_if,
	ctl_elif,
	ctl_else,
	ctl_while,
	ctl_do,
	ctl_break,
	ctl_skip,
	ctl_switch,
	ctl_case,

	ctl_namespace,
	ctl_use,
	ctl_template,
	ctl_operator_keyw,
	ctl_typedef,
};

enum ctl_operator_t {
	ctl_plus, // x+y
	ctl_min, // x-y
	ctl_div, // x/y
	ctl_mod, // x%y
	ctl_mul, // x*y

	ctl_poinc, // x++
	ctl_princ, // ++x
	ctl_podec, // x--
	ctl_prdec, // --x

	ctl_address, // $x
	ctl_dereference, // @x
	ctl_follow, // x->y ( (@x).y )
	ctl_access, // x[y]
	ctl_access2, // x.y

	ctl_gt, // x > y
	ctl_lt, // x < y
	ctl_gte, // x >= y
	ctl_lte, // x <= y
	ctl_eq, // x == y
	ctl_neq, // x != y
	
	ctl_not, // !x
	ctl_and, // x && y
	ctl_or, // x || y
	ctl_xor, // x ^^ y

	ctl_asgn, // x = y
	ctl_conv, // x:y

	ctl_nsr, // x::y
};

enum ctl_control_t {
	ctl_popen, // (
	ctl_pclose, // )
	ctl_bopen, // {
	ctl_bclose, // }
	ctl_sopen, // <
	ctl_sclose, // >
};

struct ctl_token {
	enum ctl_token_t classification;
	union {
		enum ctl_constraint_t constraint;
		enum ctl_symbol_t symbol;
		enum ctl_literal_t literal;
		enum ctl_primitive_t primitive;
		enum ctl_keyword_t keyword;
		enum ctl_operator_t operator;
		enum ctl_control_t control;
	};
};

