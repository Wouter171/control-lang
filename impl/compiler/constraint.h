enum ctl_endianness_t {ctl_native, ctl_leo, ctl_beo, ctl_leb, ctl_beb, ctl_lolb, ctl_lobb, ctl_bolb, ctl_bobb};
enum ctl_storage_t {ctl_auto, ctl_stack, ctl_reg, ctl_static};
enum ctl_mutability_t {ctl_mut, ctl_const};

struct ctl_constraints {
	enum ctl_endianness_t endian;
	enum ctl_storage_t storage;
	enum ctl_mutability_t mutability;
};

