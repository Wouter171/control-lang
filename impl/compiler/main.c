#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#include "token.h"
#include "constraint.h"

#define error(...) {fprintf(stderr, ##__VA_ARGS__);}
#define debug()	{error("Compiler: %s:%d\n", __FILE__, __LINE__);}
#define compile_fail(code) {debug(); exit(code);}


typedef uint64_t u64;
typedef uint32_t u32;
typedef uint16_t u16;
typedef uint8_t u8;

typedef int64_t i64;
typedef int32_t i32;
typedef int16_t i16;
typedef int8_t i8;

typedef enum bool_t{true=1,false=0} bool;
typedef enum chartype_t{illegal=0, letter, number, special, space} chartype;

struct ctl_type {
	struct ctl_constraints constraints;

	enum ctl_primitive_t primitive;

	union{
		struct ctl_type* oftype;
		struct ctl_struct* ofstruct;
		struct ctl_union* ofunion;
		struct ctl_enum* ofenum;
	};
};

struct ctl_noun {
	struct ctl_type type;
	u64 size;
	// (2^64)-1 == 18446744073709551615/(1024^6) ~= 16 exabytes ~= 17179869184 GB
	// should be enough, I only have 16GB ram
	u8 data[];
};

#define inrange(x,y,z) (x>=y && x<=z)

chartype classifyChar(char c){

	if        (inrange(c, '0', '9')){
		return number;
	} else if (inrange(c, '!', '/')){
		return special;
	} else if (inrange(c, ':', '@')){
		return special;
	} else if (inrange(c, '[', '`')){
		return special;
	} else if (inrange(c, '{', '~')){
		return special;
	} else if (inrange(c, 'A', 'Z')){
		return letter;
	} else if (inrange(c, 'a', 'z')){
		return letter;
	} else {
		return space;
	}
}

struct ctl_token getNextToken(u8* code, u32 remaining){
	chartype t;

	start_over:;
	
	if ((t = classifyChar(*code)) != 0)
	{
		switch (t){
			case letter:

			case number:
			case special:
			case space:
				code++;
				remaining--;
				goto start_over;

			default: {
				error("Fatal Compiler fail");
				debug();
				compile_fail(1);
			}
		}
	} else {
		error("Illegal character `%x` at -%d.", *code, remaining);
		debug();
		compile_fail(1);
	}
}

int main(int argc, char const *argv[])
{
	/* code */
	char y[] = "{aaa3232 322hh }hello;";
	const char* x[] = {"{","aaa3232","322h","h","}","hello",";"};
	char z[] = "Error unexpected 'h' near '322h'";

	/*
	
	an expression is one of:
		(literal)
		(symbol)
		(symbol)('()')
		(symbol)('=')(expression)
		(symbol)(operator)(expression)
		(symbol)('(')(expression)(')')

	a type is one of:
		(primitive)
		(contraints)(type)
		(array)(type)
		(pointer)(type)

	a statement is one of:
		declaration
			(type)(symbol)
			(type)(symbol)('=')(expression)
			(keyword)(symbol)
		expression
		controlflow
			(keyword)
			()

	*/

	return 0;
}