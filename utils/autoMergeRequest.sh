#!/usr/bin/env bash
# Based on https://about.gitlab.com/blog/2017/09/05/how-to-automatically-create-a-new-mr-on-gitlab-with-gitlab-ci/
# Extract the host where the server is running, and add the URL to the APIs
[[ $HOST =~ ^https?://[^/]+ ]] && HOST="${BASH_REMATCH[0]}/api/v4/projects/"

# Regex:
# ^ line starts here
# ? possible https
# followed by ://
# until a '/' where the capture will stop

# BASH_REMATCH contains the result of the most recent regex (~=) application


# The description of the MR
BODY="{
    \"id\": ${CI_PROJECT_ID},
    \"source_branch\": \"dev\",
    \"target_branch\": \"master\",
    \"remove_source_branch\": false,
    \"title\": \"Weekly merge to main\"
}";

# Send a POST request to the server to create a merge request
curl -X POST "${HOST}${CI_PROJECT_ID}/merge_requests" \
        --header "DEPLOY-TOKEN:${PRIVATE_TOKEN}" \
        --header "Content-Type: application/json" \
        --data "${BODY}";

echo "Opened the weekly merge request";

